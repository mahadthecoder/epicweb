# Welcome to the EpicWeb repo

# Current website build status:

[![Netlify Status](https://api.netlify.com/api/v1/badges/76191055-324b-45e8-af38-1a00752c532e/deploy-status)](https://app.netlify.com/sites/epicweb/deploys)

## All tutorials are hosted here (under CC-BY-SA License)

## THE SITE IS LICENSED UNER THE MIT LICENSE.